//
//  ShareViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 14/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class ShareViewController: BaseViewController {
    @IBOutlet var shareImageView: UIImageView!
    var shareImage: UIImage!
    var imagePath: NSURL!
    @IBOutlet var facebookButton: FBSDKShareButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        shareImageView.image = shareImage
        let photo = FBSDKSharePhoto()
        photo.image = shareImage
        photo.userGenerated = true
        photo.caption = "Preemadonna"
        let photoContent = FBSDKSharePhotoContent()
        photoContent.photos = [photo]
        facebookButton.shareContent = photoContent
        if facebookButton.enabled == false{
            UIAlertView(title: "Error", message: "To upload image you need Facebook App", delegate: self, cancelButtonTitle: "Cancel").show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionGotoHome(sender: AnyObject) {
        self.navigationController?.popToViewController(Preemadonna.sharedInstance().homecontroller, animated: true)
    }
    
    @IBAction func actionSharetoInstagram(sender: AnyObject) {
        let orientation : ALAssetOrientation = ALAssetOrientation(rawValue: shareImage!.imageOrientation.rawValue)!
                      if UIApplication.sharedApplication().canOpenURL(NSURL(string: "instagram://app")!){
                    var string = "instagram://library?AssetPath=%@&InstagramCaption=".stringByAppendingString(imagePath.absoluteString!).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                    var instagramURL = NSURL(string:string!)
                    UIApplication.sharedApplication().openURL(instagramURL!)
                }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    @IBAction func actionsharetoFacebook(sender: AnyObject) {}
    
    @IBAction func actionsharetoSnapChat(sender: AnyObject) {
        let sharingItems = [shareImage] 
        let myCustomActivity = ActivityViewCustomActivity(title: "Mark Selected", imageName: "removePin") {
            print("Do something")
        }
        let anotherCustomActivity = ActivityViewCustomActivity(title: "Reset All", imageName: "reload") {
            print("Do something else")
        }
        let activityViewController = UIActivityViewController(activityItems:sharingItems, applicationActivities:[myCustomActivity, anotherCustomActivity])
        activityViewController.excludedActivityTypes = [UIActivityTypeMail, UIActivityTypeAirDrop, UIActivityTypeMessage, UIActivityTypeAssignToContact, UIActivityTypePostToFacebook, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll]
        activityViewController.popoverPresentationController?.barButtonItem = sender as! UIBarButtonItem
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func actionPopToConfirmPrintVC(sender: AnyObject) {
        self.navigationController?.popToViewController(Preemadonna.sharedInstance().confirmPrintcontroller, animated: true)
    }
    
    @IBAction func actionsharetoPreemadonna(sender: AnyObject) {
    }
    /*
    // MARK: - Navigation
   

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
