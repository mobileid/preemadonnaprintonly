        //
//  MediaViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 31/08/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class MediaViewController: BaseViewController,RSKImageCropViewControllerDelegate,RSKImageCropViewControllerDataSource {
    
    @IBOutlet var mediaImageView: UIImageView!
    @IBOutlet var backgroundImageView: UIImageView!
    var mediaImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        backgroundImageView.image = mediaImage
        mediaImageView.hidden = true
        Preemadonna.sharedInstance().cropOption = PreemadonnaCropType.Other
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func actionSelectRound(sender: AnyObject) {
        let imageCropVC = RSKImageCropViewController(image: backgroundImageView.image)
        imageCropVC.delegate = self;
        imageCropVC.dataSource = self;
        Preemadonna.sharedInstance().cropOption = PreemadonnaCropType.Circle
        imageCropVC.cropMode = RSKImageCropMode.Circle;
        self.navigationController?.pushViewController(imageCropVC, animated: true)
    }
    
    @IBAction func actionSquare(sender: AnyObject) {
        let imageCropVC = RSKImageCropViewController(image: backgroundImageView.image)
        imageCropVC.delegate = self;
        imageCropVC.dataSource = self;
        imageCropVC.cropMode = RSKImageCropMode.Square;
        Preemadonna.sharedInstance().cropOption = PreemadonnaCropType.Square
        self.navigationController?.pushViewController(imageCropVC, animated: true)
    }
    
    @IBAction func actionNextToPrinting(sender: AnyObject) {
        self.performSegueWithIdentifier("toConfirmPrint", sender: sender)
    }
    
    func imageCropViewController(controller: RSKImageCropViewController!, didCropImage croppedImage: UIImage!, usingCropRect cropRect: CGRect) {
        self.mediaImageView.image = croppedImage;
        mediaImage = croppedImage
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func imageCropViewController(controller: RSKImageCropViewController!, didCropImage croppedImage: UIImage!, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.mediaImageView.image = croppedImage;
        mediaImage = croppedImage
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func imageCropViewController(controller: RSKImageCropViewController!, willCropImage originalImage: UIImage!) {
        mediaImageView.hidden = false
        backgroundImageView.alpha = 0.3
        backgroundImageView.contentMode = .ScaleAspectFill
        if Preemadonna.sharedInstance().cropOption == PreemadonnaCropType.Circle {
            self.mediaImageView.applyCornerRadius(self.mediaImageView.frame.height/2, BorderColor: UIColor.clearColor())
        }else {
            self.mediaImageView.applyCornerRadius(0.0, BorderColor: UIColor.clearColor())
            
        }
    }
    
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController!) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func imageCropViewControllerCustomMaskPath(controller: RSKImageCropViewController!) -> UIBezierPath! {
        let  rect :CGRect = controller.maskRect;
        let point1:CGPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
        let point2:CGPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        let  point3:CGPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
        
        let triangle: UIBezierPath = UIBezierPath()
        triangle.moveToPoint(point1)
        triangle.addLineToPoint(point2)
        triangle.addLineToPoint(point3)
        triangle .closePath()
        return nil;
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    func imageCropViewControllerCustomMaskRect(controller: RSKImageCropViewController!) -> CGRect {
        var maskSize:CGSize!
        maskSize = CGSizeMake(200, 200);
        let viewWidth:CGFloat = CGRectGetWidth(controller.view.frame);
        let viewHeight:CGFloat = CGRectGetHeight(controller.view.frame);
        var maskRect:CGRect = CGRectMake((viewWidth - maskSize.width) * 0.5,
                                         (viewHeight - maskSize.height) * 0.5,
                                         maskSize.width,
                                         maskSize.height);
        return CGRectZero;
    }
    
    func imageCropViewControllerCustomMovementRect(controller: RSKImageCropViewController!) -> CGRect {
        return CGRectZero;
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toConfirmPrint"{
            let controller = segue.destinationViewController as! ConfirmPrintViewController
            controller.image = mediaImage
            
        }
    }
    
}
