//
//  LicensedArtCell.swift
//  Preemadonna
//
//  Created by LeftRightMind on 03/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class LicensedArtCell: UICollectionViewCell {
    
    var purchaseHandler:((status:Bool) ->Void)!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var buttonFlag: UIButton!

    @IBAction func actionPurchaseLicensedArt(sender: AnyObject) {
        purchaseHandler(status: true)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.alpha = 0.5
        buttonFlag.hidden = false
    }
}
