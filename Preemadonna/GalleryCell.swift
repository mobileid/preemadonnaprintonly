//
//  GalleryCell.swift
//  Preemadonna
//
//  Created by LeftRightMind on 21/08/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    @IBOutlet var iconImageView: UIImageView!
    
}
