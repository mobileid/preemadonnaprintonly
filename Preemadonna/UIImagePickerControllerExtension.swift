//
//  UIImagePickerControllerExtension.swift
//  Preemadonna
//
//  Created by LeftRightMind on 15/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

extension UIImagePickerController{

    public override func shouldAutorotate() -> Bool {
       return false
    }
    
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.LandscapeRight
    }
}