//
//  GallaryViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 03/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class GallaryViewController: BaseViewController,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var galleryCollectionView: UICollectionView!
    var isDecresingCellSize: Bool = true
    var prevCell: GalleryCell!
    var isAnimatingFirstTym: Bool = true
    var prevIndexPath: NSIndexPath!
    var gallaryCell:GalleryCell!
    @IBOutlet var nextButton: UIButton!
    var imageArray:NSMutableArray!
    /*!
     * @brief The ViewController class' car object.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.galleryCollectionView.delegate = self
        self.galleryCollectionView.dataSource = self
        
        nextButton.enabled = false
        nextButton.alpha = 0.4
        imageArray = NSMutableArray(objects:"usa","rantenna","rcake","rdolphin","rcat","rcat2","rglares","rrobo","rsnowflakes","rvoteup","wink","snowman","ambulance","crown","camera","flower1","keys","bee","shoe","snail","carousel","music","surf","pizza","tool","money","stadium","beach","watermelon","aquarius","aries","baseball","liberty","burger","cat2","club","kiss","clover","bow","cat","chocolate","church","cityscape","firetruck","cake","chick","cancer","cross","diamond","flower2","capricon","gem","dolphin","smile1","fries","turtle","gemini","lipstick","lolipop","heart1","fish","hen","monkey","heart2","leaf","ladybug","mushroom","flower3","pig","heart","bear","football","turkey","hourglass","joker","smile2","bolt","pisces","sun2","pineapple","ribbon","pumpkin","libra","lips","snowflake","police","medal","leo","masks","mic","octopus","paws","tent","sun","taurus","sagittarius","save","strawberry","star","spade","shop","scorpio","train","umbrella","virgo")
        pageControl.numberOfPages = 8

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UICollectionViewDelegate & dataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = galleryCollectionView.dequeueReusableCellWithReuseIdentifier("GalleryCell", forIndexPath: indexPath) as! GalleryCell
        cell.iconImageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        cell.iconImageView.image?.accessibilityIdentifier =  imageArray[indexPath.row] as! String
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = galleryCollectionView.cellForItemAtIndexPath(indexPath) as! GalleryCell
        Preemadonna.sharedInstance().isImageFromCameraRoll = false
        gallaryCell = cell
        nextButton.enabled = true
        nextButton.alpha = 1.0
        if self.prevCell != cell{
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.63, 1.63))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingUp")
            
            let springAnimation:POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
            springAnimation.springBounciness = 27
            springAnimation.toValue = NSValue(CGPoint: CGPointMake(0.9, 0.9))
            springAnimation.velocity = NSValue(CGPoint: CGPointMake(2, 2))
            cell.iconImageView.pop_addAnimation(springAnimation, forKey: "springAnimation")
            
            if self.prevCell != nil{
                let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
                scaleAnimation.duration = 0.1
                scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.0, 1.0))
                prevCell.pop_addAnimation(scaleAnimation, forKey: "scalingDown")
            }
            self.prevCell = cell
        }else{
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.0, 1.0))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingDown")
            nextButton.enabled = false
            nextButton.alpha = 0.4
            gallaryCell = nil
            self.prevCell = nil
        }
    }
    
    //MARK:- IBAction
    @IBAction func actionNext(sender: AnyObject) {
        if gallaryCell != nil{
            self.popToNextControllerOfSegueIdentifier("toConfirmPrint", sender: sender)
        }
        else{}
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(galleryCollectionView.frame)
        pageControl.currentPage = Int(galleryCollectionView.contentOffset.x / pageWidth)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toConfirmPrint"{
            let controller = segue.destinationViewController as! ConfirmPrintViewController
            controller.image = gallaryCell.iconImageView.image
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
