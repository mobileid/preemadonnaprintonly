//
//  ResetPurchasesViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 15/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class ResetPurchasesViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    @IBAction func actionResetPurchases(sender: AnyObject) {
        let alertController  = UIAlertController(title:"Are you Sure?"
            , message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
        }
        alertController.addAction(cancel)
        let yes = UIAlertAction(title: "YES", style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            Preemadonna.sharedInstance().purchasedArtDataSource.removeAllObjects()
            Preemadonna.sharedInstance().save()
            self.navigationController?.popToRootViewControllerAnimated(true)
            
        }
        alertController.addAction(yes)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
