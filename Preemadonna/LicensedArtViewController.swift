//
//  LicensedArtViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 03/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class LicensedArtViewController: BaseViewController,UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var LicensedArtCollectionView: UICollectionView!
    var isDecresingCellSize: Bool = true
    var prevCell: LicensedArtCell!
    var isAnimatingFirstTym: Bool = true
    var prevIndexPath: NSIndexPath!
    var licensedArtCell:LicensedArtCell!
    @IBOutlet var nextButton: UIButton!
    var imageArray:NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.LicensedArtCollectionView.delegate = self
        self.LicensedArtCollectionView.dataSource = self
        
        nextButton.enabled = false
        nextButton.alpha = 0.4
        imageArray = NSMutableArray(objects:"fbart","hax","indiegogo","instaart","pm","rio","snap","tc","tokyo","twitter","wechat","youtube")
        pageControl.numberOfPages = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    //MARK:- UICollectionViewDelegate & dataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = LicensedArtCollectionView.dequeueReusableCellWithReuseIdentifier("LicensedArtCell", forIndexPath: indexPath) as! LicensedArtCell
        cell.iconImageView.image = UIImage(named: imageArray[indexPath.row] as! String)
        cell.iconImageView.image?.accessibilityIdentifier =  imageArray[indexPath.row] as! String
        cell.purchaseHandler =  {(status:Bool)  -> Void in
            self.LicensedArtCollectionView.reloadData()
            self.LicensedArtCollectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.None)
        }
        Preemadonna.sharedInstance().purchasedArtDataSource.containsObject(indexPath.row, handler: { (success) -> Void in
            if success == true{
                cell.buttonFlag.hidden = true
                cell.iconImageView.alpha = 1.0
            }else{
                cell.buttonFlag.hidden = false
                cell.iconImageView.alpha = 0.5
            }
        })
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = LicensedArtCollectionView.cellForItemAtIndexPath(indexPath) as! LicensedArtCell
        Preemadonna.sharedInstance().isImageFromCameraRoll = false
        licensedArtCell = cell
        nextButton.enabled = true
        nextButton.alpha = 1.0
        
        if self.prevCell != cell{
            /// Selection animation 
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.63, 1.63))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingUp")
            
            let springAnimation:POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
            springAnimation.springBounciness = 27
            springAnimation.toValue = NSValue(CGPoint: CGPointMake(0.9, 0.9))
            springAnimation.velocity = NSValue(CGPoint: CGPointMake(2, 2))
            cell.iconImageView.pop_addAnimation(springAnimation, forKey: "springAnimation")
            print("self.prevCell != cell\(indexPath.row)")
            
            // Set Purchase
            if  Preemadonna.sharedInstance().purchasedArtDataSource.count == 0{
                
                setPurchasingOfArt(indexPath.row)
            }
            Preemadonna.sharedInstance().purchasedArtDataSource.containsObject(indexPath.row, handler: { (success) -> Void in
                print("Search reult \(success)")
                print("Search reult IndexPath \(indexPath.row)")
                if success == true{
                }else{
                    self.setPurchasingOfArt(indexPath.row)
                }
            })
            if self.prevCell != nil{
                let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
                scaleAnimation.duration = 0.1
                scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.0, 1.0))
                prevCell.pop_addAnimation(scaleAnimation, forKey: "scalingDown")
                print("self.prevCell != nil \(indexPath.row)")
            }
            self.prevCell = cell
        }else{
            print("scalingDown\(indexPath.row)")
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.0, 1.0))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingDown")
            nextButton.enabled = false
            nextButton.alpha = 0.4
            licensedArtCell = nil
            self.prevCell = nil
        }
    }
    
    //MARK:- IBAction
    @IBAction func actionNext(sender: AnyObject) {
        if licensedArtCell != nil{
            self.popToNextControllerOfSegueIdentifier("toConfirmPrint", sender: sender)
        }
        else{
            
        }
    }
    
    func setPurchasingOfArt(cellNumber:Int){
        let cell  =  LicensedArtCollectionView.cellForItemAtIndexPath(NSIndexPath(forRow: cellNumber, inSection: 0)) as! LicensedArtCell
        licensedArtCell = cell
        let alertController  = UIAlertController(title:"Purchase the art to print using the Nailbot"
            , message: "This art costs $0.99", preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.0, 1.0))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingDown")
            cell.buttonFlag.hidden = false
            cell.iconImageView.alpha = 0.5
            self.nextButton.enabled = false
            self.nextButton.alpha = 0.4
            self.licensedArtCell = nil
            self.prevCell = nil
        }
        
        alertController.addAction(cancel)
        let yes = UIAlertAction(title: "PURCHASE", style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
            cell.buttonFlag.hidden = true
            cell.iconImageView.alpha = 1.0
            Preemadonna.sharedInstance().purchasedArtDataSource.addObject(cellNumber)
            Preemadonna.sharedInstance().save()
            let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPViewScaleXY)
            scaleAnimation.duration = 0.1
            scaleAnimation.toValue = NSValue(CGPoint: CGPointMake(1.63, 1.63))
            cell.pop_addAnimation(scaleAnimation, forKey: "scalingUp")
        }
        alertController.addAction(yes)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(LicensedArtCollectionView.frame)
        pageControl.currentPage = Int(LicensedArtCollectionView.contentOffset.x / pageWidth)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toConfirmPrint"{
            let controller = segue.destinationViewController as! ConfirmPrintViewController
            var image = licensedArtCell.iconImageView.image
            controller.image = licensedArtCell.iconImageView.image
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
