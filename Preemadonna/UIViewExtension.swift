//
//  UIViewExtension.swift
//
//
//  Created by LeftRightMind on 06/01/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

extension UIView {
    
    func fadeOut(value:CGFloat) {
        self.alpha = value
    }
    
    func fadeIn(value:CGFloat) {
        self.alpha = value
    }
    
    func applyCornerRadius(radius:CGFloat,BorderColor color:UIColor){
        self.layer.cornerRadius = radius
        self.layer.borderColor = color.CGColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true;
    }
    
    class func viewFromNibName(name: String) -> UIView? {
        let views = NSBundle.mainBundle().loadNibNamed(name, owner: nil, options: nil)
        return views!.first as? UIView
    }
}
