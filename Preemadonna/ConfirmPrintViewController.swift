//
//  ConfirmPrintViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 24/08/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class ConfirmPrintViewController: BaseViewController {
    
    var image:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        Preemadonna.sharedInstance().confirmPrintcontroller = self
        if Preemadonna.sharedInstance().printURL == nil {
            let alertController  = UIAlertController(title:"No device found!"
                , message: "Please configure Nailbot in settings", preferredStyle: UIAlertControllerStyle.Alert)
            let cancel = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            }
            alertController.addAction(cancel)
            let settings = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Default) { (action:UIAlertAction) -> Void in
                self.performSegueWithIdentifier("toSettings", sender: self)
            }
            alertController.addAction(settings)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    //MARK:- IBAction
    @IBAction func actionPrint(sender: AnyObject) {
        ///Airprint
        var imageData = UIImagePNGRepresentation(image)
        // Big nail size
        let vieW = UIView(frame: CGRectMake(0, 0, 612.28, 790.87))
        let imageView = UIImageView(image: image)
        if Preemadonna.sharedInstance().isImageFromCameraRoll == true{
            if Preemadonna.sharedInstance().nailRect == nil{
                Preemadonna.sharedInstance().nailRect = CAMERAROLL_BIG_NAIL_RECT
                Preemadonna.sharedInstance().nailRectCircle = CAMERAROLL_BIG_NAIL_RECT_CIRCLE
            }
            else if Preemadonna.sharedInstance().nailRect == BIG_NAIL_RECT{
                Preemadonna.sharedInstance().nailRect = CAMERAROLL_BIG_NAIL_RECT
                Preemadonna.sharedInstance().nailRectCircle = CAMERAROLL_BIG_NAIL_RECT_CIRCLE
            }
            else if  Preemadonna.sharedInstance().nailRect == SMALL_NAIL_RECT{
                Preemadonna.sharedInstance().nailRect = CAMERAROLL_SMALL_NAIL_RECT
                Preemadonna.sharedInstance().nailRectCircle = CAMERAROLL_SMALL_NAIL_RECT_CIRCLE
            }
        }else{
            if Preemadonna.sharedInstance().nailRect == nil{
                Preemadonna.sharedInstance().nailRect = BIG_NAIL_RECT
                Preemadonna.sharedInstance().nailRectCircle = BIG_NAIL_RECT_CIRCLE
                Preemadonna.sharedInstance().save()
            }
        }
        imageView.frame = Preemadonna.sharedInstance().nailRect!
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        vieW.addSubview(imageView)
        vieW.backgroundColor = UIColor.clearColor()
        imageView.backgroundColor = UIColor.clearColor()
        if Preemadonna.sharedInstance().cropOption == PreemadonnaCropType.Circle{
            imageView.frame = Preemadonna.sharedInstance().nailRectCircle!
            imageView.applyCornerRadius(CGRectGetWidth(imageView.frame)/2, BorderColor: UIColor.clearColor())
        }
        
        Preemadonna.sharedInstance().printController.printingItem = self.toPDF([vieW])
        if Preemadonna.sharedInstance().printURL != nil {
           Preemadonna.sharedInstance().printController.printToPrinter(UIPrinter(URL:Preemadonna.sharedInstance().printURL!), completionHandler: { (controller:UIPrintInteractionController, success:Bool, error:NSError?) -> Void in
                if success == true{
                    self.popToNextControllerOfSegueIdentifier("toPrint", sender: sender)
                }
                else if success == false{
                    
                }
                else if error != nil {
                }
            })
        }else{
            Preemadonna.sharedInstance().printController.presentAnimated(true, completionHandler: { (controller:UIPrintInteractionController, success:Bool, error:NSError?) -> Void in
                if success == true{
                    self.popToNextControllerOfSegueIdentifier("toPrint", sender: sender)
                }
                else if success == false{
                    
                }
                else if error != nil {
                }
            })
        }
    }
    
    private func toPDF(views: [UIView]) -> NSData? {
        //Creating a PDF
        if views.isEmpty {
            return nil
        }
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: 0, y: 0, width: 612.28, height: 790.87), nil)
        let context = UIGraphicsGetCurrentContext()
        for view in views {
            UIGraphicsBeginPDFPage()
            view.layer.renderInContext(context!)
        }
        UIGraphicsEndPDFContext()
        return pdfData
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toPrint"{
            let controller = segue.destinationViewController as! PrintViewController
            image?.accessibilityIdentifier
            controller.image = image as UIImage
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
