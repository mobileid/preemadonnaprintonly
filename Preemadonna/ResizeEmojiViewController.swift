//
//  ResizeEmojiViewController.swift
//  Preemadonna
//
//  Created by Arun Jangid on 1/11/17.
//  Copyright © 2017 Pooja Kad. All rights reserved.
//

import UIKit

class ResizeEmojiViewController: BaseViewController,UIGestureRecognizerDelegate {

    @IBOutlet var resizeImage: UIImageView!
    var pinchGesture:UIPinchGestureRecognizer!
    var rotateGesture:UIRotationGestureRecognizer!
    var panGesture: UIPanGestureRecognizer!
    var  activeRecognizers:NSMutableSet!
    var selectedTransform: CGAffineTransform!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activeRecognizers = NSMutableSet()
        resizeImage.userInteractionEnabled = true
        resizeImage.multipleTouchEnabled = true
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(actionPinchGesture))
        resizeImage.addGestureRecognizer(pinchGesture)
        
        rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(actionPinchGesture))
        resizeImage.addGestureRecognizer(rotateGesture)
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(actionPinchGesture))
        resizeImage.addGestureRecognizer(panGesture)
        rotateGesture.delegate = self
        pinchGesture.delegate = self
        panGesture.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func actionPinchGesture(sender: UIGestureRecognizer) {
        
        switch (sender.state) {
        case .Began:
            if activeRecognizers.count == 0{
                selectedTransform = resizeImage.transform;
                activeRecognizers.addObject(sender)
            }
            break;
            
        case .Ended:
            selectedTransform = self.applyRecognizer(sender, toTransform: selectedTransform);
            activeRecognizers.removeObject(sender)
            break;
            
        case .Changed:

            for recognizer in activeRecognizers{
                resizeImage.transform = self.applyRecognizer(recognizer as! UIGestureRecognizer, toTransform: selectedTransform)
            }
            break;
            
            
        default:
            break;
        }
    }
    
        

    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func applyRecognizer(recognizer:UIGestureRecognizer, toTransform:CGAffineTransform) ->CGAffineTransform
    {
        switch recognizer {
        case is UIPinchGestureRecognizer:
            
            return CGAffineTransformScale(toTransform, (recognizer as! UIPinchGestureRecognizer).scale, (recognizer as! UIPinchGestureRecognizer).scale)
            
        case is UIRotationGestureRecognizer:
            return CGAffineTransformRotate(toTransform, (recognizer as! UIRotationGestureRecognizer).rotation)
        case is UIPanGestureRecognizer:
            return CGAffineTransformTranslate(toTransform, (recognizer as! UIPanGestureRecognizer).translationInView(self.view).x, (recognizer as! UIPanGestureRecognizer).translationInView(self.view).y)
        default:
            return toTransform
            
        }
    }
}
