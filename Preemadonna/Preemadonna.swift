//
//  Preemadonna.swift
//  Preemadonna
//
//  Created by LeftRightMind on 02/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit
/**
 Image Crop Options
 
 - Circle: Cropping in circular shape
 - Square: Cropping in square shape
 - Other:  Other
 */
enum PreemadonnaCropType:String{
    case Circle = "Circle"
    case Square   = "Square"
    case Other = "Other"
}
/// Defination of big nail rect
let BIG_NAIL_RECT = CGRectMake(213.875, 19.84, 28.35, 25.51)
/// Defination of big nail rect for circular shape art
let BIG_NAIL_RECT_CIRCLE = CGRectMake(213.875, 19.84,  25.51, 25.51)
let SMALL_NAIL_RECT = CGRectMake(218.375, 26.36, 20.41, 17.01)
let SMALL_NAIL_RECT_CIRCLE = CGRectMake(218.375, 26.36,  17.01, 17.01)
let CAMERAROLL_BIG_NAIL_RECT = CGRectMake(213.875, 21.24, 25.52, 22.68)
let CAMERAROLL_BIG_NAIL_RECT_CIRCLE = CGRectMake(213.875, 21.24, 22.68, 22.68)
let CAMERAROLL_SMALL_NAIL_RECT = CGRectMake(218.375, 27.76, 17.57, 14.18)
let CAMERAROLL_SMALL_NAIL_RECT_CIRCLE = CGRectMake(218.375, 27.76, 14.18, 14.18)

/// Singleton class
class Preemadonna: NSObject {
    var  printController:UIPrintInteractionController!
    var  homecontroller:HomeViewController!
    var  confirmPrintcontroller:ConfirmPrintViewController!
    var  printURL:NSURL?
    var cropOption :PreemadonnaCropType!
    var nailRect:CGRect?
    var nailRectCircle:CGRect?
    var purchasedArtDataSource = NSMutableArray()
    var isImageFromCameraRoll:Bool!
    var documentsDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask , true)[0]
    
    class func sharedInstance() -> Preemadonna! {
        struct Static {
            static var instance: Preemadonna? = nil
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&Static.onceToken) {
            var documentsDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask , true)[0]
            Static.instance = Preemadonna(contentsOfFile:(NSURL(string: documentsDirectory)?.URLByAppendingPathComponent("Preemadonna")!.absoluteString))
            if Static.instance == nil {
                Static.instance = Preemadonna()
            }
            Static.instance?.printController = Static.instance?.initializePrinter()
        }
        return Static.instance!
    }
    
    required  override init() {
        super.init()
    }
    /**
     Saving fetched object
     
     - parameter aDecoder: NSCoder
     
     - returns: void
     */
    //MARK:- Saving fetch object
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        let string = aDecoder.decodeObjectForKey("nailSize") as! NSString
        nailRect = CGRectFromString(string as String)
        nailRectCircle = CGRectFromString(string as String)
        printURL = aDecoder.decodeObjectForKey("printURL") as? NSURL
        purchasedArtDataSource.removeAllObjects()
        purchasedArtDataSource.addObjectsFromArray(aDecoder.decodeObjectForKey("purchasedArts") as! NSMutableArray as [AnyObject])
    }
    /**
     Retriving saved object
     
     - parameter aCoder: NSCoder
     */
    override func encodeWithCoder(aCoder: NSCoder) {
        if nailRect == nil {
            let string = NSStringFromCGRect(BIG_NAIL_RECT)
            aCoder.encodeObject(string, forKey: "nailSize")
            let stringCircle =  NSStringFromCGRect(BIG_NAIL_RECT_CIRCLE) as NSString
            aCoder.encodeObject(stringCircle, forKey: "nailSizeCircle")
        }else{
            let string = NSStringFromCGRect(nailRect!)
            aCoder.encodeObject(string, forKey: "nailSize")
            let stringCircle =  NSStringFromCGRect(nailRectCircle!) as NSString
            aCoder.encodeObject(stringCircle, forKey: "nailSizeCircle")
        }
        aCoder.encodeObject(printURL, forKey: "printURL")
        aCoder.encodeObject(purchasedArtDataSource, forKey: "purchasedArts")
    }
    
    func save() {
        print(self.writeToFile((NSURL(string: documentsDirectory)?.URLByAppendingPathComponent("Preemadonna")!.absoluteString), atomically: true))
        self.writeToFile((NSURL(string: documentsDirectory)?.URLByAppendingPathComponent("Preemadonna")!.absoluteString), atomically: true)
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    /**
     Initializes and returns a printer Instance
     
     - returns: UIPrintInteractionController 
     */
    func initializePrinter() -> UIPrintInteractionController {
        printController = UIPrintInteractionController.sharedPrintController()
        // 2
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfoOutputType.General
        printInfo.printerID = "Preemadonna"
        printController.printInfo = printInfo
        print(printController.printPaper)
        // 3
        let formatter = UIMarkupTextPrintFormatter()
        formatter.contentInsets = UIEdgeInsets(top: 19.84, left:213.875 , bottom: 746.65, right: 369.775)
        printController.printFormatter = formatter
        return printController
    }
}
