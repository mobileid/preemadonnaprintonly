//
//  PreemadonnaNavigationBar.swift
//  Preemadonna
//
//  Created by LeftRightMind on 24/08/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class PreemadonnaNavigationBar: UINavigationBar {
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        //Custom Nav bar
        let siZe  = super.sizeThatFits(size)
        self.backgroundImageForBarPosition(UIBarPosition.Top, barMetrics: UIBarMetrics.Default)
        let image  = UIImage(named: "top")
        self.setBackgroundImage(image, forBarMetrics: UIBarMetrics.Default)
        return siZe
    }
   }
