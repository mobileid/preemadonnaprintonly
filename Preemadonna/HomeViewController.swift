//
//  HomeViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 31/08/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit
import AssetsLibrary
typealias CompletionHandler = (success:Bool!) -> Void

class HomeViewController: BaseViewController,ELCImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var library:ALAssetsLibrary!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        library = ALAssetsLibrary()
        Preemadonna.sharedInstance().homecontroller = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        Preemadonna.sharedInstance().cropOption = PreemadonnaCropType.Other
    }
    
    //    MARK: - IBAction
    @IBAction func actionSelectGallery(sender: AnyObject) {
        self.performSegueWithIdentifier("toGallery", sender: sender)
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    @IBAction func actionSelectLicensedArt(sender: AnyObject) {
        self.performSegueWithIdentifier("toLicensedArt", sender: sender)
    }
    
    @IBAction func actionSelectCameraRoll(sender: AnyObject) {
        let imagePicker = ELCImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumImagesCount = 1;
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    func elcImagePickerController(picker: ELCImagePickerController!, didFinishPickingMediaWithInfo info: [AnyObject]!) {
        print(info)
        print(info.count)
        if info.count > 0 {
        var imageDict = info.first as! NSDictionary
        var imageUploading = imageDict["UIImagePickerControllerOriginalImage"] as! UIImage
        Preemadonna.sharedInstance().isImageFromCameraRoll = true
        var imageData = UIImagePNGRepresentation(imageUploading)
        var lowdata = UIImageJPEGRepresentation(imageUploading, 0.5)
        var lowImage = UIImage(data: lowdata!, scale: 0.5)
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.performSegueWithIdentifier("toMediaView", sender: imageUploading)
        })
        }
    }
    
    func elcImagePickerControllerDidCancel(picker: ELCImagePickerController!) {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
        })
    }
 
    @IBAction func actionSelectInstagramRoll(sender: AnyObject) {
        self.selectImage { (success) -> Void in
        print(success)
        }
    }
    
    func selectImage(handler:CompletionHandler){
        /*-- Find Group --*/
        var groupToAddTo:ALAssetsGroup?;
        self.library?.enumerateGroupsWithTypes(ALAssetsGroupType(ALAssetsGroupAlbum),
            usingBlock: { (group:ALAssetsGroup?, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
                
                if(group != nil){
                    if group!.valueForProperty(ALAssetsGroupPropertyName) as! String == "Instagram"{
                        groupToAddTo = group;
                        
                        print("\nGroup Found \(group!.valueForProperty(ALAssetsGroupPropertyName))\n", terminator: "");
                        handler(success: true)
                        let tablePicker = ELCAssetTablePicker(style:.Plain)
                        tablePicker.singleSelection = true
                        tablePicker.immediateReturn = true
                        
                        let elcPicker = ELCImagePickerController(rootViewController: tablePicker)
                        elcPicker.maximumImagesCount = 1
                        elcPicker.imagePickerDelegate = self
                        tablePicker.parent = elcPicker;
                        
                        // Move me
                        tablePicker.assetGroup = group;
                        tablePicker.assetGroup.setAssetsFilter(ALAssetsFilter.allAssets())
                        self.presentViewController(elcPicker, animated: true, completion: nil)
                    }
                } /*Group Is Not nil*/
            },
            failureBlock: { (error:NSError!) -> Void in
                print("Failed to find group", terminator: "");
                handler(success: false)

        });
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toMediaView"{
         let controller  =  segue.destinationViewController as! MediaViewController
            controller.mediaImage = sender as! UIImage
        }
    }

}
