//
//  ConfigureNailSizeViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 01/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class ConfigureNailSizeViewController: BaseViewController {
    
    @IBOutlet var buttonSmallNail: UIButton!
    @IBOutlet var buttonBigNail: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if Preemadonna.sharedInstance().nailRect == BIG_NAIL_RECT{
            self.buttonBigNail.setImage(UIImage(named: "large"), forState:UIControlState.Normal)
            self.buttonSmallNail.setImage(UIImage(named: "smallG"), forState:UIControlState.Normal)
        } else{
            self.buttonBigNail.setImage(UIImage(named: "largeG"), forState:UIControlState.Normal)
            self.buttonSmallNail.setImage(UIImage(named: "small"), forState:UIControlState.Normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionSelectBigNailSize(sender: AnyObject) {
        if Preemadonna.sharedInstance().nailRect == BIG_NAIL_RECT{
        }else{
            self.buttonBigNail.setImage(UIImage(named:"large"), forState: UIControlState.Normal)
            self.buttonSmallNail.setImage(UIImage(named:"smallG"), forState: UIControlState.Normal)
            buttonBigNail.alpha = 0.4
            UIView.animateWithDuration(0.6, animations: { () -> Void in
                let springAnimation:POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
                springAnimation.springBounciness = 27
                springAnimation.toValue = NSValue(CGPoint: CGPointMake(0.9, 0.9))
                springAnimation.velocity = NSValue(CGPoint: CGPointMake(2, 2))
                self.buttonBigNail.pop_addAnimation(springAnimation, forKey: "springAnimation")
                self.buttonBigNail.alpha = 1.0
                }, completion: { (Bool) -> Void in
                    Preemadonna.sharedInstance().nailRect = BIG_NAIL_RECT
                    Preemadonna.sharedInstance().nailRectCircle = BIG_NAIL_RECT_CIRCLE
                    Preemadonna.sharedInstance().save()
            })
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    @IBAction func actionSelectSmallNailSize(sender: AnyObject) {
        if Preemadonna.sharedInstance().nailRect == SMALL_NAIL_RECT{
        }else{
            buttonSmallNail.alpha = 0.4
            self.buttonSmallNail.setImage(UIImage(named:"small"), forState: UIControlState.Normal)
            self.buttonBigNail.setImage(UIImage(named:"largeG"), forState: UIControlState.Normal)
            UIView.animateWithDuration(0.6, animations: { () -> Void in
                let springAnimation:POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPViewScaleXY)
                springAnimation.springBounciness = 27
                springAnimation.toValue = NSValue(CGPoint: CGPointMake(0.9, 0.9))
                springAnimation.velocity = NSValue(CGPoint: CGPointMake(2, 2))
                self.buttonSmallNail.pop_addAnimation(springAnimation, forKey: "springAnimation")
                self.buttonSmallNail.alpha = 1.0
                }, completion: { (Bool) -> Void in
                    Preemadonna.sharedInstance().nailRect = SMALL_NAIL_RECT
                    Preemadonna.sharedInstance().nailRectCircle = SMALL_NAIL_RECT_CIRCLE
                    Preemadonna.sharedInstance().save()
            })
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
