//
//  CameraViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 14/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit
import AssetsLibrary

class CameraViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var imagePicker: UIImagePickerController!
    var shareImage:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imagePicker = UIImagePickerController()
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        
        //Set Notifications so that when user rotates phone, the orientation is reset to landscape.
        //Refer to the method didRotate:
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("didRotate"), name: "UIDeviceOrientationDidChangeNotification", object: nil)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.videoQuality = UIImagePickerControllerQualityType.TypeLow
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didRotate(){
    }
    
    //MARK:- UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let imageUploading = info["UIImagePickerControllerOriginalImage"] as! UIImage
        let metadata:NSDictionary = info[UIImagePickerControllerMediaMetadata] as! NSDictionary;
        self.addImage(imageUploading, metaData: metadata, toAlbum: "Preemadonna") { (success) -> Void in
            print("Image Added : \(success)", terminator: "");
            self.performSegueWithIdentifier("toShare", sender: imageUploading)
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    func addImage(image:UIImage, metaData:NSDictionary, toAlbum albumName:String, handler:CompletionHandler){
        let  library = ALAssetsLibrary()
        library.addAssetsGroupAlbumWithName(albumName, resultBlock: {(group:ALAssetsGroup!) -> Void in
            print("\nAlbum Created:=  \(albumName)", terminator: "");
            /*-- Find Group --*/
            var groupToAddTo:ALAssetsGroup?;
            library.enumerateGroupsWithTypes(ALAssetsGroupType(ALAssetsGroupAlbum),
                usingBlock: { (group:ALAssetsGroup?, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
                    if(group != nil){
                        if group!.valueForProperty(ALAssetsGroupPropertyName) as! String == albumName{
                            groupToAddTo = group;
                            print("\nGroup Found \(group!.valueForProperty(ALAssetsGroupPropertyName))\n", terminator: "");
                            library.writeImageToSavedPhotosAlbum(image.CGImage, metadata:metaData as [NSObject : AnyObject], completionBlock: {(assetURL:NSURL!,error:NSError!) -> Void in
                                if(error == nil){
                                    library.assetForURL(assetURL,
                                        resultBlock: { (asset:ALAsset!) -> Void in
                                            let yes:Bool? = groupToAddTo?.addAsset(asset);
                                            if (yes == true){
                                                handler(success: true);
                                            }
                                        },
                                        failureBlock: { (error2:NSError!) -> Void in
                                            print("Failed to add asset", terminator: "");
                                            handler(success: false);
                                    });
                                }
                            });
                        }
                    } /*Group Is Not nil*/
                },
                failureBlock: { (error:NSError!) -> Void in
                    print("Failed to find group", terminator: "");
                    handler(success: false);
            });
            
            }, failureBlock: { (error:NSError!) -> Void in
                print("Failed to create \(error)", terminator: "");
                handler(success: false);
        });
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toMediaView"{
            let controller  =  segue.destinationViewController as! ShareViewController
            controller.shareImage = sender as! UIImage
        }
    }
    
}
