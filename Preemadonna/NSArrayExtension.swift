//
//  NSArrayExtension.swift
//  Preemadonna
//
//  Created by LeftRightMind on 03/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

extension NSArray {
    
    func containsObject(givenObject:AnyObject!,handler:CompletionHandler){
        self.enumerateObjectsUsingBlock { (object:AnyObject!, index:Int, stop) -> Void in
            if givenObject.isEqual(object) {
                handler(success: true)
                stop.initialize(true)
            }else{
                if index == self.count - 1{
                    handler(success: false)
                    stop.initialize(true)
                    
                }
                
            }
        }
        
    }
    
    
}
