//
//  LoginViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 02/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet var textFieldUsername: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionLogin(sender: AnyObject) {
        self.performSegueWithIdentifier("toHome", sender: sender)
    }
    
    @IBAction func actionLoginWithFacebook(sender: AnyObject) {
    }
    
    @IBAction func actionLoginWithInstagram(sender: AnyObject) {
    }
    
    @IBAction func actionLoginWithSnapChat(sender: AnyObject) {
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    
}
