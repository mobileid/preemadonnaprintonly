//
//  SettingsViewController.swift
//  Preemadonna
//
//  Created by LeftRightMind on 01/09/15.
//  Copyright (c) 2015 LeftRightMind. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController,UIPrinterPickerControllerDelegate {
    @IBOutlet var buttonNailSize: UIButton!
    @IBOutlet var buttonNailPrinterSettings: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if Preemadonna.sharedInstance().nailRect == BIG_NAIL_RECT{
            self.buttonNailSize.setImage(UIImage(named: "sizelarge"), forState:UIControlState.Normal)
        } else{
            self.buttonNailSize.setImage(UIImage(named: "sizesmall"), forState:UIControlState.Normal)
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeRight
    }
    
    @IBAction func actionSetNailSize(sender: AnyObject) {
    }
    
    @IBAction func actionSetPrinter(sender: AnyObject) {
        //Presetting Printer
        var PrinterPicker = UIPrinterPickerController(initiallySelectedPrinter: nil)
        PrinterPicker.delegate = self
        PrinterPicker.presentAnimated(true, completionHandler: { (printer:UIPrinterPickerController, success:Bool, error:NSError?) -> Void in
            print(printer)
            if error == nil && success ==  true{
                PrinterPicker = UIPrinterPickerController(initiallySelectedPrinter: printer.selectedPrinter)
                print(PrinterPicker.selectedPrinter?.URL)
                Preemadonna.sharedInstance().printURL = PrinterPicker.selectedPrinter?.URL
                Preemadonna.sharedInstance().save()
            }
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
